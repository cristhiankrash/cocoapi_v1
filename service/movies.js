const MongoLib = require('../lib/mongo')

class MovieService {
    constructor() {
        this.collection = 'movies'
        this.mongoDB = new MongoLib()
    }

    async getMovies(params) {
        const params = params && { query: { $in: params } }
        const movies = await this.mongoDB.getAll(this.collection, query)
        return movies || []
    }

    async getMovies(movieId) {
        const movie = await this.mongoDB.get(this.collection, movieId)
        return movie || {}
    }

    async createMovie(movie) {
        const newMovie = await this.mongoDB.get(this.collection, movie)
        return newMovie

    }

    async updateMovie(movieId, movie) {
        const movie = await this.mongoDB.update(this.collection, movieId, movie)
        return movie
    }

    async updateMovie(movieId) {
        const movieId = await this.mongoDB.delete(this.collection, movieId)
        return movieId
    }
}

module.exports = MovieService