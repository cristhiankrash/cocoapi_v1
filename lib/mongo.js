const { MongoClient, ObjectId } = require('mongodb')
const { config } = require('../config')

const MONGO_URI = `mongodb+srv://${config.DB_USER}:${config.DB_PASSWORD}@${config.DB_HOST}:${config.port}/${config.DB_NAME}?retryWrite=true&w=majoruty`

class MongoLib {
    constructor() {
        this.client = new MongoClient(MONGO_URI, { useNewUrlParser: true });
    }
    //patron singlenton para que solo se cree una isntancia y no sature o cree erores al futuro
    connect() {
        if (!MongoLib.connection) {
            MongoLib.connection = new Promise((resolve, reject) => {
                // si existe devuelva el error como reject
                this.client.connect(err => !err ? resolve(this.client.db(this.DB_NAME)) : reject(err))
            })
        }
        // si no existe la intancia entonces la creeo si escxiste no la creeo
        return MongoLib.connection
    }

    getAll(collection, query) {
        return this.connect().then(db => {
            return db.collection(collection).find(query).toArray()
        })
    }

    get(collection, id) {
        return this.connect().then(db => {
            return db.collection(collection).findOne({ _id: ObjectId(id) })
        })
    }

    create(collection, data) {
        return this.connect().then(db => {
            return db.collection(collection).insertOne(data)
        }).then(result => result.insertedId);
    }

    update(collection, id, data) {
        return this.connect().then(db => {
            return db.collection(collection).updateOne(
                { _id: ObjectId(id) },
                { $set: data },
                { upsert: true }
            )
        }).then(result => result.upsertedId || id);
    }

    delete(collection, id) {
        return this.connect().then(db => {
            return db.collection(collection).deleteOne(data)
        }).then(result => result._id)
    }
}


module.exports = MongoLib