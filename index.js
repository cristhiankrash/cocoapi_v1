const express = require('express')
const app = express();
const { config } =  require('./config/index')
const moviesControl = require('./service/movies')

moviesControl(app)

// body parse
app.use(express.json())

app.listen(config.port), ()=> {
	console.log(`Listening in port ${config.port}`)
}